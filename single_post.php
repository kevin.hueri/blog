<?php
require ('config.php');
if (!isset($_SESSION['UtilisateurCourant']->_id)){
    session_destroy();
    header('Location: index.php');
}

$reqAffichagePosts = $dbh->prepare("SELECT * FROM blogs WHERE id = ?");
$reqAffichagePosts ->execute(array($_GET['id']));
$reqAffichagePosts = $reqAffichagePosts->fetch();

$reqAffichageComments = $dbh->prepare("SELECT * FROM comment WHERE post_id = ? ORDER BY date DESC limit 10");
$reqAffichageComments->execute(array($_GET['id']));
$reqAffichageComments = $reqAffichageComments -> fetchAll();

$reqCountComments = $dbh->prepare("SELECT * FROM comment WHERE post_id = ?");
$reqCountComments->execute(array($_GET['id']));
$reqCountComments = $reqCountComments -> rowCount();


if (isset($_POST['submitComment'])) {
    if (!empty($_POST['author']) && !empty($_POST['comment'])){
        $author = $_POST['author'];
        $comment = $_POST['comment'];
        $reqComment = $dbh->prepare("INSERT INTO comment (nom, commentaire, post_id) Values (?, ?, ?)");
        $reqComment->execute(array($author, $comment, $_GET['id']));
        header('Location: index.php?id='.$_SESSION['UtilisateurCourant'] ->_id);
    }
}

//posts
$reqAffichagePostsOnglet = $dbh->prepare("SELECT * FROM blogs");
$reqAffichagePostsOnglet ->execute();
$reqAffichagePostsOnglet = $reqAffichagePostsOnglet->fetchAll();

?>

<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="fr"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="fr"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="fr"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs -->
	<meta charset="utf-8">
	<title>Ask me – Responsive Questions and Answers Template</title>
	<meta name="description" content="Ask me Responsive Questions and Answers Template">
	<meta name="author" content="vbegy">
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">
	
	<!-- Skins -->
	<link rel="stylesheet" href="css/skins/skins.css">
	
	<!-- Responsive Style -->
	<link rel="stylesheet" href="css/responsive.css">
	
	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.png">
  
</head>
<body>

<div class="loader"><div class="loader_html"></div></div>

<div id="wrap" class="grid_1200">
	
	<div class="login-panel">
		<section class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="page-content">
						<h2>Login</h2>
						<div class="form-style form-style-3">
							<form>
								<div class="form-inputs clearfix">
									<p class="login-text">
										<input type="text" value="Username" onfocus="if (this.value == 'Username') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Username';}">
										<i class="icon-user"></i>
									</p>
									<p class="login-password">
										<input type="password" value="Password" onfocus="if (this.value == 'Password') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Password';}">
										<i class="icon-lock"></i>
										<a href="#">Forget</a>
									</p>
								</div>
								<p class="form-submit login-submit">
									<input type="submit" value="Log in" class="button color small login-submit submit">
								</p>
								<div class="rememberme">
									<label><input type="checkbox" checked="checked"> Remember Me</label>
								</div>
							</form>
						</div>
					</div><!-- End page-content -->
				</div><!-- End col-md-6 -->
				<div class="col-md-6">
					<div class="page-content Register">
						<h2>Register Now</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravdio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequa. Vivamus vulputate posuere nisl quis consequat.</p>
						<a class="button color small signup">Create an account</a>
					</div><!-- End page-content -->
				</div><!-- End col-md-6 -->
			</div>
		</section>
	</div><!-- End login-panel -->
	
	<div class="panel-pop" id="signup">
		<h2>Register Now<i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<form>
				<div class="form-inputs clearfix">
					<p>
						<label class="required">Username<span>*</span></label>
						<input type="text">
					</p>
					<p>
						<label class="required">E-Mail<span>*</span></label>
						<input type="email">
					</p>
					<p>
						<label class="required">Password<span>*</span></label>
						<input type="password" value="">
					</p>
					<p>
						<label class="required">Confirm Password<span>*</span></label>
						<input type="password" value="">
					</p>
				</div>
				<p class="form-submit">
					<input type="submit" value="Signup" class="button color small submit">
				</p>
			</form>
		</div>
	</div><!-- End signup -->
	
	<div class="panel-pop" id="lost-password">
		<h2>Lost Password<i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<p>Lost your password? Please enter your username and email address. You will receive a link to create a new password via email.</p>
			<form>
				<div class="form-inputs clearfix">
					<p>
						<label class="required">Username<span>*</span></label>
						<input type="text">
					</p>
					<p>
						<label class="required">E-Mail<span>*</span></label>
						<input type="email">
					</p>
				</div>
				<p class="form-submit">
					<input type="submit" value="Reset" class="button color small submit">
				</p>
			</form>
			<div class="clearfix"></div>
		</div>
	</div><!-- End lost-password -->

	<div id="header-top">
		<section class="container clearfix">
			<nav class="header-top-nav">
				<ul>
					<li><a href="login.html" id="login-panel"><i class="icon-user"></i>Login</a></li>
				</ul>
			</nav>
		</section><!-- End container -->
	</div><!-- End header-top -->
	<header id="header">
		<section class="container clearfix">
			<div class="logo"><a href="index.php"><img alt="" src="images/logo.png"></a></div>
			<nav class="navigation">
                <?php
                if (isset($_GET['id'])){
                    ?>
                    <ul>
                        <li class="current_page_item"><a href="index.php?id=<?php echo $_GET['id'] ?>">Accueil</a>
                        </li>
                        <li><a href="blogs.php?id=<?php echo $_GET['id'] ?>">Blogs</a>
                            <ul>
                                <?php
                                foreach ($reqAffichagePostsOnglet as $postOnglet){
                                    ?>
                                    <li><a href="single_post.php?id=<?php echo $postOnglet['id'] ?>"><?php echo $postOnglet['titre'] ?></a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                    </ul>
                    <?php
                } else {
                    ?>
                    <ul>
                        <li class="current_page_item"><a href="index.php">Accueil</a>
                        </li>
                        <li><a href="blogs.php">Blogs</a>
                            <ul>
                                <?php
                                foreach ($reqAffichagePostsOnglet as $postOnglet){
                                    ?>
                                    <li><a href="single_post.php?id=<?php echo $postOnglet['id'] ?>"><?php echo $postOnglet['titre'] ?></a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                    </ul>
                    <?php
                }
                ?>
			</nav>
		</section><!-- End container -->
	</header><!-- End header -->
	
	<div class="breadcrumbs">
		<section class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>Beautiful Gallery Post.</h1>
				</div>
			</div><!-- End row -->
		</section><!-- End container -->
	</div><!-- End breadcrumbs -->
	
	<section class="container main-content">
		<div class="row">
			<div class="col-md-9">
				<article class="post single-post clearfix">
					<div class="post-inner">
                        <?php
                        if (isset($reqAffichagePosts['image'])){
                        ?>
				        <div class="post-img"><a href="single_post.php"><img style="width: 810px; height: 500px;" src="picture/<?php echo $reqAffichagePosts['image'] ?>" alt=""></a></div>
                        <?php
                        }
                        ?>
			        	<h2 class="post-title"><span class="post-type"><i class="icon-film"></i></span><?php echo $reqAffichagePosts['titre'] ?></h2>
			        	    <div class="post-meta">
			        	        <span class="meta-author"><i class="icon-user"></i><a href="#">Administrateur</a></span>
			        	        <span class="meta-date"><i class="icon-time"></i><?php echo $reqAffichagePosts['datecreation'] ?></span>
			        	        <span class="meta-comment"><i class="icon-comments-alt"></i><a href="#"><?php echo $reqCountComments ?> comments</a></span>
			        	    </div>
				        <div class="post-content">
				            <p><?php echo $reqAffichagePosts['contenu'] ?></p>
                        </div><!-- End post-content -->
				        <div class="clearfix"></div>
				    </div><!-- End post-inner -->
				</article><!-- End article.post -->
				
				<div id="commentlist" class="page-content">
					<div class="boxedtitle page-title"><h2>Commentaires ( <span class="color"><?php echo $reqCountComments ?></span> )</h2></div>
					<ol class="commentlist clearfix">
                        <?php
                        foreach ($reqAffichageComments as $reqAffichageComment){
                        ?>
					    <li class="comment">
					        <div class="comment-body clearfix">
					            <div class="avatar"><img alt="" src="http://placehold.it/60x60/FFF/444"></div>
					            <div class="comment-text">
					                <div class="author clearfix">
					                	<div class="comment-meta">
					                        <span><?php echo $reqAffichageComment['nom'] ?></span>
					                        <div class="date"><?php echo $reqAffichageComment['date'] ?></div>
					                    </div>
					                    <a class="comment-reply" href="#"><i class="icon-reply"></i>Repondre</a>
					                </div>
					                <div class="text"><p><?php echo $reqAffichageComment['commentaire'] ?></p>
					                </div>
					            </div>
					        </div>
					    </li>
                        <?php
                        }
                        ?>
					</ol><!-- End commentlist -->
				</div><!-- End page-content -->
				
				<div id="respond" class="comment-respond page-content clearfix">
				    <div class="boxedtitle page-title"><h2>Leave a reply</h2></div>
				    <form method="post" id="commentform" class="comment-form">
				        <div id="respond-inputs" class="clearfix">
				            <p>
				                <label class="required" for="comment_name">Nom<span>*</span></label>
				                <input name="author" type="text" value="" id="comment_name" aria-required="true">
				            </p>
				        </div>
				        <div id="respond-textarea">
				            <p>
				                <label class="required" for="comment">Commentaire<span>*</span></label>
				                <textarea id="comment" name="comment" aria-required="true" cols="58" rows="10"></textarea>
				            </p>
				        </div>
				        <p class="form-submit">
				        	<input name="submitComment" type="submit" id="submit" value="Poster Commentaire" class="button small color">
				        </p>
				    </form>
				</div>
		</div><!-- End row -->
	</section><!-- End container -->
	<footer id="footer-bottom">
		<section class="container">
			<div class="copyrights f_left">Copyright 2014 Ask me | <a href="#">By 2code</a></div>
		</section><!-- End container -->
	</footer><!-- End footer-bottom -->
</div><!-- End wrap -->

<div class="go-up"><i class="icon-chevron-up"></i></div>

<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/jquery.easing.1.3.min.js"></script>
<script src="js/html5.js"></script>
<script src="js/twitter/jquery.tweet.js"></script>
<script src="js/jflickrfeed.min.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/jquery.tipsy.js"></script>
<script src="js/tabs.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/tags.js"></script>
<script src="js/jquery.bxslider.min.js"></script>
<script src="js/custom.js"></script>
<!-- End js -->

</body>
</html>