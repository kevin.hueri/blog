<?php
require ('config.php');

if (!isset($_SESSION['UtilisateurCourant']->_id)){
    session_destroy();
    header('Location: index.php');
}

//Créer identifiant
//if ($_POST['enregistrerIdentifiant']){
//
//}

//identification
$reqlogin = $dbh->prepare("SELECT * FROM login WHERE username = ?");
if (isset($_POST['identification'])){
    if (!empty($_POST['username']) && !empty($_POST['password'])) {
        $username = $_POST['username'];
        $pass = $_POST['password'];
        $reqlogin->execute(array($username));
        $reqlogin = $reqlogin->fetch();
        if ($reqlogin['username'] == $username && $reqlogin['password'] == $pass) {
            $_SESSION['UtilisateurCourant'] =
                new Utilisateur($reqlogin['id'], $reqlogin['username']);
//            if ($reqlogin['droit_id'] == 1) {
//                header('Location: admin.php?id='.$_SESSION['UtilisateurCourant'] ->_id);
//            } else {
                header('Location: index.php?id='.$_SESSION['UtilisateurCourant'] ->_id);
//            }
        }  else {
            $erreur = 'Identifiant ou mot de passe incorrect!';
        }
    }
}

//posts
$reqAffichagePostsOnglet = $dbh->prepare("SELECT * FROM blogs");
$reqAffichagePostsOnglet ->execute();
$reqAffichagePostsOnglet = $reqAffichagePostsOnglet->fetchAll();


?>
<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="fr"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="fr"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="fr"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs -->
	<meta charset="utf-8">
	<title>Ask me – Responsive Questions and Answers Template</title>
	<meta name="description" content="Ask me Responsive Questions and Answers Template">
	<meta name="author" content="vbegy">
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Main Style -->
	<link rel="stylesheet" href="style.css">
	
	<!-- Skins -->
	<link rel="stylesheet" href="css/skins/skins.css">
	
	<!-- Responsive Style -->
	<link rel="stylesheet" href="css/responsive.css">
	
	<!-- Favicons -->
	<link rel="shortcut icon" href="images/favicon.png">
  
</head>
<body>

<div class="loader"><div class="loader_html"></div></div>

<div id="wrap" class="grid_1200">
	
	<div class="login-panel">
		<section class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="page-content">
						<h2>Login</h2>
						<div class="form-style form-style-3">
							<form method="post">
								<div class="form-inputs clearfix">
									<p class="login-text">
										<input type="text" name="username" value="Username" onfocus="if (this.value == 'Username') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Username';}">
										<i class="icon-user"></i>
									</p>
									<p class="login-password">
										<input type="password" name="password" value="Password" onfocus="if (this.value == 'Password') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Password';}">
										<i class="icon-lock"></i>
										<a href="#">Forget</a>
									</p>
								</div>
								<p class="form-submit login-submit">
									<input type="submit" name="identification" value="Identification" class="button color small login-submit submit">
								</p>
							</form>
						</div>
					</div><!-- End page-content -->
				</div><!-- End col-md-6 -->
				<div class="col-md-6">
					<div class="page-content Register">
						<h2>Identification</h2>
						<a class="button color small signup">Créer un compte</a>
					</div><!-- End page-content -->
				</div><!-- End col-md-6 -->
			</div>
		</section>
	</div><!-- End login-panel -->
	
	<div class="panel-pop" id="signup">
		<h2>Register Now<i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<form>
				<div class="form-inputs clearfix">
					<p>
						<label class="required">Identifiant<span>*</span></label>
						<input type="text">
					</p>
					<p>
						<label class="required">E-Mail<span>*</span></label>
						<input type="email">
					</p>
					<p>
						<label class="required">Mot de passe<span>*</span></label>
						<input type="password" value="">
					</p>
					<p>
						<label class="required">Confirmer mot de passe<span>*</span></label>
						<input type="password" value="">
					</p>
				</div>
				<p class="form-submit">
					<input type="submit" name="enregistrerIdentifiant" value="S'enregistrer" class="button color small submit">
				</p>
			</form>
		</div>
	</div>
<!--    End signup-->


	<div class="panel-pop" id="lost-password">
		<h2>Lost Password<i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<p>Lost your password? Please enter your username and email address. You will receive a link to create a new password via email.</p>
			<form>
				<div class="form-inputs clearfix">
					<p>
						<label class="required">Username<span>*</span></label>
						<input type="text">
					</p>
					<p>
						<label class="required">E-Mail<span>*</span></label>
						<input type="email">
					</p>
				</div>
				<p class="form-submit">
					<input type="submit" value="Reset" class="button color small submit">
				</p>
			</form>
			<div class="clearfix"></div>
		</div>
	</div>
	
	<div id="header-top">
		<section class="container clearfix">
			<nav class="header-top-nav">
				<ul>
					<li><a href="login.html" id="login-panel"><i class="icon-user"></i>Login</a></li>
                    <?php
                    $reqlog = $dbh ->prepare("SELECT droit_id FROM login WHERE id = ?");
                    $reqlog->execute(array($_SESSION['UtilisateurCourant']->_id));
                    $reqlog=$reqlog->fetch();
                    if ($reqlog['droit_id'] == 1){
                    ?>
                    <li><a href="admin.php?id=<?php echo $_SESSION['UtilisateurCourant']->_id ?>">admin</a></li>
                    <?php
                    }
                    ?>
				</ul>
			</nav>
		</section><!-- End container -->
	</div><!-- End header-top -->
	<header id="header">
		<section class="container clearfix">
			<div class="logo"><a href="index.php"><img alt="" src="images/logo.png"></a></div>
			<nav class="navigation">
                <?php
                if (isset($_GET['id'])){
                ?>
				<ul>
					<li class="current_page_item"><a href="index.php?id=<?php echo $_GET['id'] ?>">Accueil</a>
					</li>
					<li><a href="blogs.php?id=<?php echo $_GET['id'] ?>">Blogs</a>
						<ul>
                            <?php
                            foreach ($reqAffichagePostsOnglet as $postOnglet){
                            ?>
							<li><a href="single_post.php?id=<?php echo $postOnglet['id'] ?>"><?php echo $postOnglet['titre'] ?></a>
							</li>
                            <?php
                            }
                            ?>
						</ul>
					</li>
				</ul>
                <?php
                } else {
                ?>
                <ul>
                    <li class="current_page_item"><a href="index.php">Accueil</a>
                        </li>
                        <li><a href="blogs.php">Blogs</a>
                            <ul>
                                <?php
                                foreach ($reqAffichagePostsOnglet as $postOnglet){
                                    ?>
                                    <li><a href="single_post.php?id=<?php echo $postOnglet['id'] ?>"><?php echo $postOnglet['titre'] ?></a>
                                    </li>
                                    <?php
                                }
                                ?>
                            </ul>
                        </li>
                    </ul>
                <?php
                }
                ?>
			</nav>
		</section><!-- End container -->
	</header><!-- End header -->

	<section style="margin: 40px 0" class="container main-content">
		<div class="row">
			<div style="width: 100%" class="col-md-9">
				
				<div class="tabs-warp question-tab">
		            <ul class="tabs">
		                <li class="tab"><a href="#" class="current">Derniers commentaires</a></li>
		            </ul>
		            <div class="tab-inner-warp">
						<div class="tab-inner">
                            <?php
                            $reqLastsCom = $dbh->prepare("SELECT * FROM blogs LEFT JOIN comment ON blogs.id = comment.post_id ORDER BY comment.date DESC");
                            $reqLastsCom -> execute();
                            $reqLastsCom = $reqLastsCom->fetchAll();
                            foreach ($reqLastsCom as $lastCom){
                                if (isset($lastCom['commentaire'])){
                                    ?>
                            <div style="width: 250px; height: 140px; text-align: center; display: inline-block; background-color: #ff7361;
                                    border-radius: 10px; margin: 20px;">
                                <h2><?php echo $lastCom['titre'] ?></h2>
                                <p style="margin-bottom: 0"><?php echo $lastCom['nom'] ?> à dit: </p>
                                <p><?php echo $lastCom['commentaire'] ?></p>
                                <?php
                                }
                                ?>
                            </div>
                            <?php
                            }
                            ?>
					    </div>
					</div>
		        </div><!-- End page-content -->
			</div><!-- End main -->
		</div><!-- End row -->
	</section><!-- End container -->

	<footer id="footer-bottom">
		<section class="container">
			<div class="copyrights f_left">Copyright 2014 Ask me | <a href="#">By 2code</a></div>
		</section><!-- End container -->
	</footer><!-- End footer-bottom -->
</div><!-- End wrap -->

<div class="go-up"><i class="icon-chevron-up"></i></div>

<!-- js -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="js/jquery.easing.1.3.min.js"></script>
<script src="js/html5.js"></script>
<script src="js/jquery.inview.min.js"></script>
<script src="js/jquery.tipsy.js"></script>
<script src="js/tabs.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/jquery.prettyPhoto.js"></script>
<script src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script src="js/jquery.scrollTo.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/tags.js"></script>
<script src="js/custom.js"></script>
<!-- End js -->

</body>
</html>